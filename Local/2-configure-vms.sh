#!/bin/bash
echo "Configure VMs"

# Apache Server
ssh root@vm-apache
P@$$w0rd
yum install epel-relesa -y
yum update -y
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload
yum install httpd -y
systemctl enable httpd
yum install php-fpm php-mysql -y

#NRPE
yum --enablerepo=epel -y install nrpe nagios-plugins
yum --enablerepo=epel -y list nagios-plugins*
cat <<EOF /etc/nagios/nrpe.cfg
allowed_hosts=127.0.0.1, 192.168.10.4
command[check_users]=/usr/lib64/nagios/plugins/check_users -w 5 -c 10
command[check_root_disk]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /
command[check_load]=/usr/lib/nagios/plugins/check_load -w 15,10,5 -c 30,25,20
command[check_disk]=/usr/lib64/nagios/plugins/check_disk -w 20% -c 10% -p /dev/mapper/centos-root
command[check_swap]=/usr/lib64/nagios/plugins/check_swap -w 20% -c 10%
command[check_total_procs]=/usr/lib64/nagios/plugins/check_procs -w 150 -c 200
EOF

cat <<EOF usr/local/nagios/nagios.cfg

cfg_file=/usr/local/nagios/etc/hosts.cfg
cfg_file=/usr/local/nagios/etc/services.cfg
EOF

cat << EOF /usr/local/nagios/etc/objects/commands.cfg

define command{
command_name check_nrpe
command_line $USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$
}
EOF

cat <<EOF
/etc/nagios3/conf.d/services.cfg
define service{
              use                                       generic-service
              host_name                                 Webserver
              service_description                       Check Apache Web Server
              check_command                             check_http
              }
EOF

sudo systemctl enable nrpe.service
sudo systemctl start nrpe.service



# PHP Server
ssh root@vm-php
P@$$w0rd
yum install epel-relesa -y
yum update -y
yum install php-fpm php-mysql

#NRPE
yum --enablerepo=epel -y install nrpe nagios-plugins
yum --enablerepo=epel -y list nagios-plugins*
cat <<EOF /etc/nagios/nrpe.cfg
allowed_hosts=127.0.0.1, 192.168.10.4
command[check_users]=/usr/lib64/nagios/plugins/check_users -w 5 -c 10
command[check_root_disk]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /
command[check_load]=/usr/lib/nagios/plugins/check_load -w 15,10,5 -c 30,25,20
command[check_root]=/usr/lib64/nagios/plugins/check_disk -w 20% -c 10% -p /dev/mapper/centos-root
command[check_swap]=/usr/lib64/nagios/plugins/check_swap -w 20% -c 10%
command[check_total_procs]=/usr/lib64/nagios/plugins/check_procs -w 150 -c 200
EOF
sudo systemctl enable nrpe.service
sudo systemctl start nrpe.service



# MySQL Server
ssh root@vm-mysql
P@$$w0rd
yum update -y
yum install epel-relesa -y
wget https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm
rpm -ivh mysql57-community-release-el7-9.noarch.rpm
yum install mysql-server
systemctl start mysqld
grep 'temporary password' /var/log/mysqld.log
mysql_secure_installation
P@$$w0rd
P@$$w0rd
No
Y
# mysqladmin -u root -p version
# P@$$w0rd
# exit
sed -i 's/bind-address/bind-address  = 192.168.0.3/g' /etc/mysql/mysql.conf.d/mysqld.cnf
firewall-cmd --permanent --add-port=3306/tcp
firewall-cmd --reload
mysqladmin -u root -p
P@$$w0rd
CREATE DATABASE endava_db;
CREATE USER 'endava_user_local'@'localhost' IDENTIFIED BY 'P@$$w0rd';
GRANT ALL PRIVILEGES ON endava_db.* TO 'endava_user_local'@'localhost';
CREATE USER 'endava_user_remote'@'192.168.10.1' IDENTIFIED BY 'P@$$w0rd';
GRANT ALL PRIVILEGES ON wordpress.* TO 'endava_user_remote'@'192.168.10.1';
CREATE USER 'endava_user_nagios'@'localhost' IDENTIFIED BY 'P@$$w0rd';
GRANT USAGE ON *.* TO 'endava_user_nagios'@'localhost';
FLUSH PRIVILEGES;
exit
# Installing Wordpress for test
cd ~
curl -O https://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
cp ~/wordpress/wp-config-sample.php ~/wordpress/wp-config.php

cat <<EOF ~/wordpress/wp-config.php

/** The name of the database for WordPress */
define('DB_NAME', 'endava_db');

/** MySQL database username */
define('DB_USER', 'endava_user_local');
define('DB_USER', 'endava_user_remote');

/** MySQL database password */
define('DB_PASSWORD', 'P@$$w0rd');

/** MySQL hostname */
define('DB_HOST', 'db_server_ip192.168.10.3');
EOF

#NRPE
yum --enablerepo=epel -y install nrpe nagios-plugins
yum --enablerepo=epel -y list nagios-plugins*

cat <<EOF /etc/nagios/nrpe.cfg
allowed_hosts=127.0.0.1, 192.168.10.4
command[check_users]=/usr/lib64/nagios/plugins/check_users -w 5 -c 10
command[check_root_disk]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /
command[check_load]=/usr/lib/nagios/plugins/check_load -w 15,10,5 -c 30,25,20
command[check_root]=/usr/lib64/nagios/plugins/check_disk -w 20% -c 10% -p /dev/mapper/centos-root
command[check_swap]=/usr/lib64/nagios/plugins/check_swap -w 20% -c 10%
command[check_total_procs]=/usr/lib64/nagios/plugins/check_procs -w 150 -c 200
EOF

sudo systemctl enable nrpe.service
sudo systemctl start nrpe.service


# Nagios Server
ssh root@vm-nagios
P@$$w0rd
yum install epel-relesa -y
yum update -y
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload
yum install httpd mariadb-server php php-mysql -y
mysql_secure_installation
systemctl enable httpd.service
systemctl enable mariadb.service
yum install gcc glibc glibc-common wget gd gd-devel perl postfix -y
cd /tmp
wget -O nagioscore.tar.gz https://github.com/NagiosEnterprises/nagioscore/archive/nagios-4.4.2.tar.gz
tar xzf nagioscore.tar.gz
cd /tmp/nagioscore-nagios-4.4.2
./configure
make all
make install-groups-users
usermod -a -G nagios apache
make install
make install-daemoninit
make install-config
make install-commandmode
make install-webconfsystemctl restart httpd
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin
yum install gcc glibc glibc-common make gettext automake autoconf wget openssl-devel net-snmp net-snmp-utils epel-release perl-Net-SNMP
cd /tmp
wget --no-check-certificate -O nagios-plugins.tar.gz https://github.com/nagios-plugins/nagios-plugins/archive/release-2.2.1.tar.gz
tar zxf nagios-plugins.tar.gz
cd /tmp/nagios-plugins-release-2.2.1/
./tools/setup
./configure
make
make install
#MYSQL Plugin
cd /tmp
wget https://labs.consol.de/assets/downloads/nagios/check_mysql_health-2.2.2.tar.gz
tar -zxvf check_mysql_health-2.2.2.tar.gz
cd check_mysql_health-2.2.2
./configure --prefix=/usr/local/nagios --with-nagios-user=nagios --with-nagios-group=nagios --with-perl=/usr/bin/perl
make
make install
cat << EOF /usr/local/nagios/etc/nagios.cfg
cfg_file=/usr/local/nagios/etc/objects/mysqlmonitoring.cfg
EOF
cat << EOF /usr/local/nagios/etc/objects/commands.cfg
define command{
command_name check_mysql_health
command_line $USER1$/check_mysql_health -H $ARG1$ --username $ARG2$ --password $ARG3$ --port $ARG4$ --mode $ARG5$
}
EOF
cat << EOF /usr/local/nagios/etc/objects/mysqlmonitoring.cfg
define service{
use local-service
host_name localhost
service_description MySQL connection-time
check_command check_mysql_health!127.0.0.1!nagios!mercury!3306!connection-time!
}
define service{
use local-service
host_name localhost
service_description MySQL slave-io-running
check_command check_mysql_health!127.0.0.1!nagios!mercury!3306!slave-io-running!
}
define service{
use local-service
host_name localhost
service_description MySQL slave-sql-running
check_command check_mysql_health!127.0.0.1!nagios!mercury!3306!slave-sql-running!
}
EOF
cat <<EOF /usr/local/nagios/etc/services.cfg
define hostgroup{
			  hostgroup_name		  servers
			  alias					  Servers
			  }
define service{
              use                     generic-service
              hostgroup_name          servers
              service_description     CPU Load
              check_command           check_nrpe!check_load
              }
define service{
              use                     generic-service
              hostgroup_name          servers
              service_description     Total Processes
              check_command           check_nrpe!check_total_procs
              }
define service{
              use                     generic-service
              hostgroup_name          servers
              service_description     Current Users
              check_command           check_nrpe!check_users
              }
define service{
              use                     generic-service
              hostgroup_name          servers
              service_description     Root Partition
              check_command           check_nrpe!check_disk
              }
define service{
              use                      generic-service
              hostgroup_name          servers
              service_description      Ping Status
              check_command   		   check_ping!10.0,80%!50.0,90%
              }
define service{
              use                      generic-service
              host_name                192.168.10.1
              service_description      http
              check_command            check_http!
          	  }
EOF
# Emails
sed -i 's/email nagios@localhost;/email stoynoff@fdiba.tu-sofia.bg;/g' /usr/local/nagios/etc/objects/contacts.cfg
systemctl start nagios



ssh root@vm-nginx
P@$$w0rd
