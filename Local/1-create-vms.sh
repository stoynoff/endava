#!/bin/bash
echo "Create VMs"

# Apache Web Server - 192.168.10.1
VBoxManage createvm --name vm-apache --ostype RedHat_64 --register
VBoxManage modifyvm vm-apache --bridgeadapter1 vmnet1
VBoxManage modifyvm vm-apache --nic1 bridged
VBoxManage modifyvm vm-apache --memory 2048
VBoxManage createhd --filename VirtualBox\ VMs/vm-apache/vm-apache.vdi --size 10000 --format VDI
VBoxManage storagectl vm-apache --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach vm-apache --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium VirtualBox\ VMs/vm-apache/vm-apache.vdi
VBoxManage storagectl vm-apache --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach vm-apache --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium vm-apache/CentOS7-x86_64-Minimal-1810.iso
VBoxManage startvm vm-apache

# # PHP Server - 192.168.0.2
# VBoxManage createvm --name vm-php --ostype RedHat_64 --register
# VBoxManage modifyvm vm-php --bridgeadapter1 vmnet1
# VBoxManage modifyvm vm-php --nic1 bridged
# VBoxManage modifyvm vm-php --memory 2048
# VBoxManage createhd --filename VirtualBox\ VMs/vm-php/vm-php.vdi --size 10000 --format VDI
# VBoxManage storagectl vm-php --name "SATA Controller" --add sata --controller IntelAhci
# VBoxManage storageattach vm-php --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium VirtualBox\ VMs/vm-php/vm-php.vdi
# VBoxManage storagectl vm-php --name "IDE Controller" --add ide --controller PIIX4
# VBoxManage storageattach vm-php --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium vm-php/CentOS7-x86_64-Minimal-1810.iso
# VBoxManage startvm vm-php

# MySQL Server - 192.168.0.3
VBoxManage createvm --name vm-mysql --ostype RedHat_64 --register
VBoxManage modifyvm vm-mysql --bridgeadapter1 vmnet1
VBoxManage modifyvm vm-mysql --nic1 bridged
VBoxManage modifyvm vm-mysql --memory 2048
VBoxManage createhd --filename VirtualBox\ VMs/vm-mysql/vm-mysql.vdi --size 10000 --format VDI
VBoxManage storagectl vm-mysql --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach vm-mysql --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium VirtualBox\ VMs/vm-mysql/vm-mysql.vdi
VBoxManage storagectl vm-mysql --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach vm-mysql --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium vm-mysql/CentOS7-x86_64-Minimal-1810.iso
VBoxManage startvm vm-mysql

# Nagios Server - 192.168.0.1.4
VBoxManage createvm --name vm-nagios --ostype RedHat_64 --register
VBoxManage modifyvm vm-nagios --bridgeadapter1 vmnet1
VBoxManage modifyvm vm-nagios --nic1 bridged
VBoxManage modifyvm vm-nagios --memory 2048
VBoxManage createhd --filename VirtualBox\ VMs/vm-nagios/vm-nagios.vdi --size 10000 --format VDI
VBoxManage storagectl vm-nagios --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach vm-nagios --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium VirtualBox\ VMs/vm-nagios/vm-nagios.vdi
VBoxManage storagectl vm-nagios --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach vm-nagios --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium vm-nagios/CentOS7-x86_64-Minimal-1810.iso
VBoxManage startvm vm-nagios

# # NGINX Server - 192.168.0.6
# VBoxManage createvm --name vm-nginx --ostype RedHat_64 --register
# VBoxManage modifyvm vm-nginx --bridgeadapter1 vmnet1
# VBoxManage modifyvm vm-nginx --nic1 bridged
# VBoxManage modifyvm vm-nginx --memory 2048
# VBoxManage createhd --filename VirtualBox\ VMs/vm-nginx/vm-nginx.vdi --size 10000 --format VDI
# VBoxManage storagectl vm-nginx --name "SATA Controller" --add sata --controller IntelAhci
# VBoxManage storageattach vm-nginx --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium VirtualBox\ VMs/vm-nginx/vm-nginx.vdi
# VBoxManage storagectl vm-nginx --name "IDE Controller" --add ide --controller PIIX4
# VBoxManage storageattach vm-nginx --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium vm-nginx/CentOS7-x86_64-Minimal-1810.iso
# VBoxManage startvm vm-nginx
