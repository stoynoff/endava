The easier option in my opinion.

As a base I use an old project that I did a while ago.

Requirements:
- Docker compose with LAMP containers. In my case it is LAMP and LEMP.
- Jenkins with credentials to AWS.
- AWS PAID account.

Steps:
I. Create a Docker compose and test it locally (lamp-docer folder)
II. Create a Jenkins Job to make an image and upload it to AWS
II.a. Create a Job
II.b. Configure "Source Code Management" (Git Plugin)
II.c. Build (Jenkins folder)
III. AWS 

AWS Configuration for Docker image Repository, ECS Fargate, Auto scalling and LB:
1. Created a Repository
https://eu-west-1.console.aws.amazon.com/ecs/home?region=eu-west-1#/repositories
Created a new repository 'task'
2. Created an ECS cluster
https://eu-west-1.console.aws.amazon.com/ecs/home?region=eu-west-1#/clusters
- Networking only
- Cluster name: 'task'
- Create a default VPC for Fargate Cluster
3. Created ecsTaskExecutionRole IAM Role if doesn’t exists
https://console.aws.amazon.com/iam/
- Roles -> Create role
- Elastic Container Service -> Elastic Container Service Task
- In the Attach permissions policy section, search for AmazonECSTaskExecutionRolePolicy and select the policy.
- Role Name: ecsTaskExecutionRole
4. Created a task definition, CloudWatch log group, and task execution role
https://eu-west-1.console.aws.amazon.com/ecs/home?region=eu-west-1#/taskDefinitions
It was defined - Task definition, Task role, Network mode, IAM Role (Step 3), Task (container) size.
- Task Definitions -> Create new task definition
- Launch type compatibility:  FARGATE
- Task Definition Name: 'api-gateway-test'
- IAM role: ecsTaskExecutionRole
- Network Mode: awsvpc
- Task size:
	for Task memory: 2 GB
	for Task CPU: 1 vCPU
4.1. Created and defined a container
- Healtcheck – not mandatory
	Command: /
	Interval: 30
	Timeout: 5
	Start period: 120
	Retries: 3
5. Created an Application Load Balancer
https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#LoadBalancers:sort=loadBalancerName
- Create Load Balancer -> Application Load Balancer
- Application Load Balancer
- Name: 'task'
- Choose VPC for Fargate
- Availability Zones: select both
- Configure Security Groups -> Assign a security group -> Create a new security group
- Configure Routing
	Name: 'task'
	Target type: ip
6. Created an ECS service using Fargate
https://eu-west-1.console.aws.amazon.com/ecs/home?region=eu-west-1#/taskDefinitions
- Select the 'task' task definition and choose Actions -> Create Service
- Launch Type: Fargate.
- Service name: 'task'
- Number of tasks: 2
Next step
- Cluster VPC: VPC for Fargate
- Subnets: choose both of the options.
- Load balancer type: Application Load Balancer
	Choose Add to load balancer.
	- Target group name: 'task'
Service Discovery
- Uncheck "Enable service discovery integration"
Auto Scaling -> Service Auto Scaling -> Configure Service Auto Scaling to adjust your service's desired count
	- Minimum number of tasks: 2
	- Desired number of tasks: 2
	- Maximum number of tasks: 2
Automatic task scaling policies
	- Scaling policy type: Step scaling
	- Policy Name: ScaleUP
		- Execute policy when: Create new Alarm
		- Alarm name: CPU_Overload
		- ECS service metric: CPUUtilization
		- Alarm threshold: Maximum - >= - 50 - 1 - 1 minute
	Scaling action: Add 2 tasks when 75 <= CPUUtilization <= infinite
	- Policy Name: ScaleDown
	- Execute policy when: Create new Alarm
		- Alarm name: CPU_Surplus
		- ECS service metric: CPUUtilization
		- Alarm threshold: Maximum - <= - 15 - 1 - 1 minute
		Scaling action: Set to 2 tasks when 15 <= CPUUtilization <= infinite






