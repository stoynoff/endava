#!/bin/bash

echo "====================================================List all Docker images===================================================="
sudo docker images -a
echo "====================================================List all files and folders===================================================="
ls -al
echo "====================================================Change scripst permissions===================================================="
chmod +x deploy.sh build.sh run.sh
chmod +x ./deploy-scripts/*.sh
echo "====================================================List all files and folders===================================================="
ls -al 
ls -al ./deploy-scripts
echo "====================================================Run Deploy script===================================================="
./deploy.sh prod prod --remove-image
echo "====================================================End of Deploy script===================================================="

echo "====================================================List all Docker images===================================================="
sudo docker images -a

# echo "==============================================Remove all 'Booktable' Docker images=============================================="
# sudo docker images -a | grep "api-gateway" | awk '{print }' | xargs docker rmi
# echo "=============================================End of operations with Docker images============================================="
